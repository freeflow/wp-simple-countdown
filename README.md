# Wordpress Simple Countdown

### How to use
Use the shortcode `countdown` with a date attribute in (YYYY-MM-DD HH:MM:SS) format to specify a date and time to be the target countdown. Here are some examples of what the shortcode would  look like in the body copy:
```
[countdown date="2020/09/01"]
[countdown date="2020/09/01 13:00"]
[countdown date="2020/09/01 05:00"]
[countdown date="2020/09/01 17:25:30"]
```

*forked from https://github.com/cscalzo/wp-countdown-timer
