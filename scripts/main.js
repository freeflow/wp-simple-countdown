var timer;

function ready(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

function pad(n) {
  return (n < 10 ? '0' : '') + n;
}

function getCountdown(el, target_date) {
  var days, hours, minutes, seconds;
  var liDays = el.querySelector('.days');
  var liHours = el.querySelector('.hours');
  var liMinutes = el.querySelector('.minutes');
  var liSeconds = el.querySelector('.seconds');

  var current_date = new Date().getTime();
  var seconds_left = (target_date - current_date) / 1000;

  if (isNaN(target_date) || seconds_left <= 0) {
    clearInterval(timer);
    liDays.innerText = '00';
    liHours.innerText = '00';
    liMinutes.innerText = '00';
    liSeconds.innerText = '00';
  } else {
    days = pad(parseInt(seconds_left / 86400, 10));
    seconds_left = seconds_left % 86400;

    hours = pad(parseInt(seconds_left / 3600, 10));
    seconds_left = seconds_left % 3600;

    minutes = pad(parseInt(seconds_left / 60, 10));
    seconds = pad(parseInt(seconds_left % 60, 10));

    liDays.innerHTML = '<span>' + days + '</span>';
    liHours.innerHTML = '<span>' + hours + '</span>';
    liMinutes.innerHTML = '<span>' + minutes + '</span>';
    liSeconds.innerHTML = '<span>' + seconds + '</span>';
  }
}

ready(function() {
  // Countdown_timer
  var countdown = document.querySelector('.countdown-timer-wrapper');

  if (countdown) {
    var target_date = new Date(countdown.getAttribute('data-event-date')).getTime();
    getCountdown(countdown, target_date);

    timer = setInterval(function() {
      getCountdown(countdown, target_date);
    }, 1000);
  }
});
