<?php

/**
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wearefreeflow.com
 * @since             1.0.0
 * @package           Wp_Simple_Countdown
 *
 * @wordpress-plugin
 * Plugin Name:       WP Simple Countdown
 * Plugin URI:        https://gitlab.com/freeflow/wp-simple-countdown
 * Description:       Create countdown timer with shortcode
 * Version:           1.0.0
 * Author:            Freeflow
 * Author URI:        https://wearefreeflow.com
 * License:           MIT
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

// Include assets
function wp_simple_countdown_init_assets(){
    wp_enqueue_script('wp_simple_countdown', plugin_dir_url( __FILE__ ) . 'scripts/main.js', array(), '1.0.0', false);
    wp_enqueue_style('wp_simple_countdown', plugin_dir_url(__FILE__) . 'styles/main.css', array(), '1.0.0');
}
add_action('wp_enqueue_scripts', 'wp_simple_countdown_init_assets');

// Define shortcode
function wp_simple_countdown_shortcode($atts){
	// Pull/Set Paramaters
    $default_bg = plugin_dir_url( __FILE__ ) . 'styles/bg-countdown.png';

	$args =  shortcode_atts(
        array(
            'date' => '2020-01-01T00:00:01',
            'bg'   => $default_bg,
            'small' => false
        ), $atts);

	$date = $args['date'];
	$bg = empty($args['bg']) ? $default_bg : $args['bg'];
	$small = !empty($args['small']);

	$wrapper_class = 'countdown-timer-wrapper';
    if ($small) {
        $wrapper_class .= ' small';
    }

	// Generate HTML
	$html = '';
	$html .= '<div class="'.$wrapper_class.'" data-event-date="'.$date.'" style="background-image: url('.$bg.');">';
	$html .=    '<ul class="countdown-timer-ul">';
	$html .=        '<li class="countdown-timer-font-size"><span class="days countdown-timer-span-size">00</span><br>DAYS</li>';
	$html .=        '<li class="countdown-timer-colon-size">:</li>';
	$html .=        '<li class="countdown-timer-font-size"><span class="hours countdown-timer-span-size">00</span><br>HOURS</li>';
	$html .=        '<li class="countdown-timer-colon-size">:</li>';
	$html .=        '<li class="countdown-timer-font-size"><span class="minutes countdown-timer-span-size">00</span><br>MINUTES</li>';
	$html .=        '<li class="countdown-timer-colon-size">:</li>';
	$html .=        '<li class="countdown-timer-font-size"><span class="seconds countdown-timer-span-size">00</span><br>SECONDS</li>';
	$html .=    '</ul>';
	$html .= '</div>';

	return $html;
}
add_shortcode('countdown', 'wp_simple_countdown_shortcode');
